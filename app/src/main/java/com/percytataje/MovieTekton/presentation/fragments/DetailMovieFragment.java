package com.percytataje.MovieTekton.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.percytataje.MovieTekton.R;
import com.percytataje.MovieTekton.core.BaseActivity;
import com.percytataje.MovieTekton.core.BaseFragment;
import com.percytataje.MovieTekton.data.model.MovieModel;
import com.percytataje.MovieTekton.presentation.activities.DetailMovieActivity;
import com.percytataje.MovieTekton.presentation.activities.ExampleActivity;
import com.percytataje.MovieTekton.presentation.contracts.MainContract;
import com.percytataje.MovieTekton.utils.ProgressDialogCustom;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by junior on 27/08/16.
 */
public class DetailMovieFragment extends BaseFragment {

    private static final String TAG = ExampleActivity.class.getSimpleName();

    @BindView(R.id.tv)
    TextView tv;

    private MovieModel mMovieModel;
    private MainContract.Presenter mPresenter;

    private ProgressDialogCustom mProgressDialogCustom;



    public DetailMovieFragment() {

    }

    public static DetailMovieFragment newInstance(MovieModel movieModel)
    {
        DetailMovieFragment fragment = new DetailMovieFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DetailMovieActivity.MOVIE_EXTRA, movieModel);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mMovieModel = (MovieModel) bundle.getSerializable(DetailMovieActivity.MOVIE_EXTRA);

    }


    @Override
    public void onResume() {
        super.onResume();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail_movie, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Ingresando...");
        tv.setText(mMovieModel.getOverview());

    }


}
