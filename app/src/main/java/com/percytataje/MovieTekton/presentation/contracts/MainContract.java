package com.percytataje.MovieTekton.presentation.contracts;

import com.percytataje.MovieTekton.core.BasePresenter;
import com.percytataje.MovieTekton.core.BaseView;
import com.percytataje.MovieTekton.presentation.presenters.MoviesPresenter;

/**
 * Especifica el contrato entre la vista y el presentador para logueo
 */
public interface MainContract {

    interface View extends BaseView<Presenter> {


        boolean isActive();


    }

    interface Presenter extends BasePresenter {


    }
}
