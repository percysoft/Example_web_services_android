package com.percytataje.MovieTekton.presentation.presenters;

import android.content.Context;
import android.support.annotation.NonNull;

import com.percytataje.MovieTekton.BuildConfig;
import com.percytataje.MovieTekton.R;
import com.percytataje.MovieTekton.data.ServiceFactory;
import com.percytataje.MovieTekton.data.local.SessionManager;
import com.percytataje.MovieTekton.data.model.MovieModel;
import com.percytataje.MovieTekton.data.model.holder.TrackEntityHolder;
import com.percytataje.MovieTekton.data.remote.request.MoviesRequest;
import com.percytataje.MovieTekton.presentation.contracts.MainContract;
import com.percytataje.MovieTekton.presentation.contracts.MovieContract;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesPresenter implements MovieContract.Presenter{

    private final MovieContract.View mView;
    private final SessionManager mSessionManager;
    private boolean mFirstLoad = false;
    private Context mContext;
    private int mTotalPages;

    public MoviesPresenter(@NonNull MovieContract.View view,
                           Context context) {
        this.mView = view;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
        this.mContext = context;

    }


    @Override
    public void start() {
        if (!mFirstLoad) {
            int a = 1;
            loadPopularMovies(a);
            mFirstLoad = true;
            a++;
        }
    }


    @Override
    public void loadPopularMovies(final int page) {
        mView.setLoadingIndicator(true);
        final MoviesRequest moviesRequest =
                ServiceFactory.createService(MoviesRequest.class);
        String languaje = mContext.getResources().getString(R.string.languaje_api);
        Call<TrackEntityHolder<MovieModel>> call = moviesRequest.
                getMoviesPopular(BuildConfig.API_KEY, languaje,page);

        call.enqueue(new Callback<TrackEntityHolder<MovieModel>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<MovieModel>> call, Response<TrackEntityHolder<MovieModel>> response) {
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (!mView.isActive()) {
                        return;
                    }

                    mTotalPages = response.body().getTotal_pages();

                    if (page==1){
                        mView.showPopularMovies(response.body().getResults());

                    }else{
                        mView.showMorePopularMoviews(response.body().getResults());
                    }

                } else {
                    if (!mView.isActive()) {
                        return;
                    }
                    mView.showErrorMessage("No se pudieron cargar las peliculas");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<MovieModel>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
            }
        });
    }
}
