package com.percytataje.MovieTekton.presentation.contracts;

import com.percytataje.MovieTekton.core.BasePresenter;
import com.percytataje.MovieTekton.core.BaseView;
import com.percytataje.MovieTekton.data.model.MovieModel;

import java.util.ArrayList;

/**
 * Especifica el contrato entre la vista y el presentador para logueo
 */
public interface MovieContract {

    interface View extends BaseView<Presenter> {
        void showPopularMovies(ArrayList<MovieModel> movieModels);

        void showMorePopularMoviews(ArrayList<MovieModel> movieModels);

        boolean isActive();

        void showDetail(MovieModel entity);


    }

    interface Presenter extends BasePresenter {

        void loadPopularMovies(int page);

    }
}
