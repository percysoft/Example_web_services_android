package com.percytataje.MovieTekton.presentation.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.percytataje.MovieTekton.R;
import com.percytataje.MovieTekton.core.BaseActivity;
import com.percytataje.MovieTekton.core.BaseFragment;
import com.percytataje.MovieTekton.core.EndlessRecyclerOnScrollListener;
import com.percytataje.MovieTekton.core.ScrollChildSwipeRefreshLayout;
import com.percytataje.MovieTekton.data.model.MovieModel;
import com.percytataje.MovieTekton.presentation.activities.DetailMovieActivity;
import com.percytataje.MovieTekton.presentation.activities.ExampleActivity;
import com.percytataje.MovieTekton.presentation.adapters.MoviesAdapter;
import com.percytataje.MovieTekton.presentation.contracts.MovieContract;
import com.percytataje.MovieTekton.presentation.presenters.MoviesPresenter;
import com.percytataje.MovieTekton.presentation.presenters.communicators.CommunicatorEntityItem;
import com.percytataje.MovieTekton.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieFragment extends BaseFragment  implements  MovieContract.View{

    private static final String TAG = ExampleActivity.class.getSimpleName();
    @BindView(R.id.complatins_list)
    RecyclerView complatinsList;
    @BindView(R.id.noItems)
    LinearLayout noItems;

    private MovieContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private GridLayoutManager gridLayoutManager;
    private static final int NUMBER_COLUM = 2;
    private MoviesAdapter moviesAdapter;

    public MovieFragment() {
        // Requires empty public constructor
    }

    public static MovieFragment newInstance() {
        return new MovieFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter =  new MoviesPresenter(this,getContext());
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_movie, container, false);
        ButterKnife.bind(this, root);
        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) root.findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(complatinsList);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadPopularMovies(1);
            }
        });
        return root;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        moviesAdapter =  new MoviesAdapter(new ArrayList<MovieModel>(),getContext(), (CommunicatorEntityItem<MovieModel>) mPresenter);
        gridLayoutManager =  new GridLayoutManager(getContext(),NUMBER_COLUM);
        complatinsList.setAdapter(moviesAdapter);
        complatinsList.setLayoutManager(gridLayoutManager);

    }


    @Override
    public void setPresenter(MovieContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) getView().findViewById(R.id.refresh_layout);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });
    }

    @Override
    public void showMessage(String msg) {
        ((BaseActivity) getActivity()).showMessage(msg);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }


    @Override
    public void showPopularMovies(ArrayList<MovieModel> movieModels) {
        if (movieModels != null){
            moviesAdapter.setItems(movieModels);
            if (movieModels.size()>0){
                noItems.setVisibility(View.GONE);
            }else{
                noItems.setVisibility(View.VISIBLE);
            }
        }else{
            showErrorMessage("Ocurrió un error desconocido");
        }
        complatinsList.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                mPresenter.loadPopularMovies(current_page);
            }
        });

    }

    @Override
    public void showMorePopularMoviews(ArrayList<MovieModel> movieModels) {
        moviesAdapter.addMoreItems(movieModels);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void showDetail(MovieModel entity) {
        Intent intent = new Intent(getContext(), DetailMovieActivity.class);
        intent.putExtra(DetailMovieActivity.MOVIE_EXTRA,entity);
        startActivity(intent);
    }



}
