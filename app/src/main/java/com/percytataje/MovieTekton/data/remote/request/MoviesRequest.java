package com.percytataje.MovieTekton.data.remote.request;

import com.percytataje.MovieTekton.data.ApiConstants;
import com.percytataje.MovieTekton.data.model.MovieModel;
import com.percytataje.MovieTekton.data.model.holder.TrackEntityHolder;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by percysoft on 27/11/16.
 */

public interface MoviesRequest {


    @GET(ApiConstants.POPULAR_MOVIES)
    Call<TrackEntityHolder<MovieModel>> getMoviesPopular(@Query("api_key") String apiKey,
                                                         @Query("language") String languaje,
                                                         @Query("page") int page);


}
